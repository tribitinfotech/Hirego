//
//  ViewController.swift
//  Tabbar
//
//  Created by Ketan on 7/26/17.
//  Copyright © 2017 kETANpATEL. All rights reserved.
//

import UIKit

class MenuController: UIViewController, UIPageViewControllerDataSource,UIPageViewControllerDelegate,UIScrollViewDelegate {
    
    
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblMessages: UILabel!
    @IBOutlet weak var lblBooking: UILabel!
    @IBOutlet weak var lblHome: UILabel!
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet private weak var btnTab1: UIButton!
    @IBOutlet private weak var btnTab2: UIButton!
    @IBOutlet private weak var btnTab3: UIButton!
    @IBOutlet private weak var btnTab5: UIButton!
    
    @IBOutlet private weak var btnTab11: UIButton!
    @IBOutlet private weak var btnTab22: UIButton!
    @IBOutlet private weak var btnTab33: UIButton!
    @IBOutlet private weak var btnTab55: UIButton!
    
    var searchVC:SearchViewController! = nil
    var bookingVC:BookingViewController! = nil
    var messageVC:MessageViewController! = nil
    var profileVC:ProfileViewController! = nil
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    @IBOutlet private weak var constantViewLeft: NSLayoutConstraint!
    @IBOutlet private weak var viewLine: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
  
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         NotificationCenter.default.addObserver(self, selector: #selector(endEditing), name: Notification.Name("endEditing"), object: nil)
        
        currentPage = 0
        self.btnTab11.isSelected = true
        self.btnTab1.backgroundColor = UIColor.init(red: 67.0/255.0, green: 226.0/255.0, blue: 48.0/255.0, alpha: 1.0)
        lblHome.textColor = UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0)
        createPageViewController()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Custom Methods
    @objc func endEditing()
    {
        pageController.setViewControllers([arrVC[5]], direction: UIPageViewControllerNavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        
        currentPage = 5
        
        btnTab1.backgroundColor = UIColor.clear
        btnTab2.backgroundColor = UIColor.clear
        btnTab3.backgroundColor = UIColor.clear
        btnTab5.backgroundColor = UIColor.clear
        
        btnTab11.isSelected = false
        btnTab22.isSelected = false
        btnTab33.isSelected = false
        btnTab55.isSelected = false
    }
    private func selectedButton(btn: UIButton) {
        
        btn.setTitleColor(UIColor.white, for: .normal)
        
       // constantViewLeft.constant = btn.frame.origin.x
        
        self.view.layoutIfNeeded()
        
    }
    
    private func unSelectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
    }

    //MARK: - IBaction Methods
    
    
    @IBAction func btnMenuAction(_ sender: Any)
    {
        
    }
    @IBAction private func btnOptionClicked(btn: UIButton) {
        
        print(btn.tag - 1)
        
        if btn.tag - 1 == 0 {
            
            NotificationCenter.default.post(name: Notification.Name("resetAll"), object: nil)
        }
        
        pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: {(Bool) -> Void in
        })
        
        resetTabBarForTag(tag: btn.tag-1)
    }
    
    //MARK: - CreatePagination
    
    private func createPageViewController() {
        
        pageController = UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        //pageController.dataSource = self
        
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        
        if UIDevice.current.iPhoneX {
            print("This device is a iPhoneX")
            
            self.pageController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-89)
        }
        else
        {
            self.pageController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-54)
        }
        
        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        searchVC = homeStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        bookingVC = homeStoryboard.instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
        messageVC = homeStoryboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        profileVC = homeStoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
        arrVC = [bookingVC,searchVC,messageVC,profileVC]
        
        pageController.setViewControllers([bookingVC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        self.addChildViewController(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)
        
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
   
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage)
        }
    }
    

    
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    
    private func resetTabBarForTag(tag: Int) {
        
        var sender: UIButton!
        
        if(tag == 0) {
            sender = btnTab1
        }
        else if(tag == 1) {
            sender = btnTab2
        }
        else if(tag == 2) {
            sender = btnTab3
        }
     
        else if(tag == 3) {
            sender = btnTab5
        }
        
        currentPage = tag
        
        unSelectedButton(btn: btnTab1)
        unSelectedButton(btn: btnTab2)
        unSelectedButton(btn: btnTab3)
        unSelectedButton(btn: btnTab5)
        
        unSelectedButton(btn: btnTab11)
        unSelectedButton(btn: btnTab22)
        unSelectedButton(btn: btnTab33)
        unSelectedButton(btn: btnTab55)
        
        btnTab1.backgroundColor = UIColor.clear
        btnTab2.backgroundColor = UIColor.clear
        btnTab3.backgroundColor = UIColor.clear
        btnTab5.backgroundColor = UIColor.clear
        
        btnTab11.isSelected = false
        btnTab22.isSelected = false
        btnTab33.isSelected = false
        btnTab55.isSelected = false
        
        lblHome.textColor = UIColor.white
        lblBooking.textColor = UIColor.white
        lblProfile.textColor = UIColor.white
        lblMessages.textColor = UIColor.white
        
        sender.backgroundColor = UIColor.init(red: 67.0/255.0, green: 226.0/255.0, blue: 48.0/255.0, alpha: 1.0)
        
        if sender == btnTab1 {
            btnTab11.isSelected = true
            lblHome.textColor = UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0)
        }
        if sender == btnTab2 {
            btnTab22.isSelected = true
            lblBooking.textColor = UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0)
        }
        if sender == btnTab3 {
            btnTab33.isSelected = true
            lblMessages.textColor = UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0)
        }
        if sender == btnTab5 {
            btnTab55.isSelected = true
            lblProfile.textColor = UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0)
        }
        
        selectedButton(btn: sender)
        
    }
    
    //MARK: - UIScrollView Delegate Methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let xFromCenter: CGFloat = self.view.frame.size.width-scrollView.contentOffset.x
        //let xCoor: CGFloat = CGFloat(viewLine.frame.size.width) * CGFloat(currentPage)
        //let xPosition: CGFloat = xCoor - xFromCenter/CGFloat(arrVC.count)
        //constantViewLeft.constant = xPosition
    }
    
}
extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
}
