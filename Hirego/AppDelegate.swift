//
//  AppDelegate.swift
//  Hirego
//
//  Created by Jitendra bhadja on 26/06/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var destIndex:NSInteger?
    
    static let shared:AppDelegate = UIApplication.shared.delegate as! AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
    
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        if UserDefaults.standard.bool(forKey: "firstTime") == false
        {
            UserDefaults.standard.set(true, forKey: "firstTime")
            UserDefaults.standard.setValue("", forKey: "userId")
            UserDefaults.standard.synchronize()
        }
        
        return true
    }

    func loginUser()
    {
        let strURL:String = MainDomain + login
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(UserDefaults.standard.value(forKey: "username") as! String, forKey: "user")
        parameter.setValue(UserDefaults.standard.value(forKey: "password") as! String, forKey: "password")
        
        print(parameter)
        
        var jsondata2 = Data()
        
        do {
            jsondata2 = try JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted)
            // use jsonData
        } catch {
            // report error
        }
        
        request.httpBody = jsondata2
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                return
            }
            
            let jsonResult = String(decoding: data, as: UTF8.self)
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                return
            }
            
            let jsonData = jsonResult.data(using: .utf8)
            let dictionary = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
            print(dictionary!)
            
            UserDefaults.standard.set(((dictionary as! NSDictionary).value(forKey: "user") as! NSDictionary).value(forKey: "userId") as! String, forKey: "userId")
            
            let dictResponseOffline:NSData = NSKeyedArchiver.archivedData(withRootObject: dictionary ?? "") as NSData
            
            let date = Date()
            
            let secondPress = date.timeIntervalSinceReferenceDate
            
            UserDefaults.standard.set(dictResponseOffline, forKey: "userData")
            UserDefaults.standard.set(secondPress, forKey: "timeStamp")
            UserDefaults.standard.synchronize()
            
            NotificationCenter.default.post(name: Notification.Name("callWebserviceAfterRenewToken"), object: nil)
            
            print(jsonResult)
        })
        
        dataTask.resume()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

