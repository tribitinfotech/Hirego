//
//  LoginViewController.swift
//  Hirego
//
//  Created by Jitendra bhadja on 18/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView

class RegisterViewController: UIViewController {

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    var obj:InitialViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SKActivityIndicator.spinnerColor(UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0))
        SKActivityIndicator.statusTextColor(UIColor.black)
        let myFont = UIFont(name: "AvenirNext-DemiBold", size: 18)
        SKActivityIndicator.statusLabelFont(myFont!)
        
        self.btnRegister.layer.cornerRadius = 25
        
        self.customizeTextfield(textField: self.txtEmail)
        self.customizeTextfield(textField: self.txtPassword)
        self.customizeTextfield(textField: self.txtUserName)
        self.customizeTextfield(textField: self.txtFirstName)
        self.customizeTextfield(textField: self.txtLastName)
        self.customizeTextfield(textField: self.txtPhoneNumber)
        
        // Do any additional setup after loading the view.
    }

    //MARK:- Private Method
    
    func customizeTextfield(textField:UITextField)
    {
        textField.layer.borderColor = UIColor.init(red: 67.0/255.0, green: 226.0/255.0, blue: 49.0/255.0, alpha: 1.0).cgColor
        
        textField.layer.borderWidth = 1.0
        
        textField.layer.cornerRadius = 25
    }
    
    //MARK:- Action zones
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRegisterAction(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if self.txtEmail.text == "" {
            Alert.show("Please enter email", self)
            return
        }
        if self.txtUserName.text == "" {
            Alert.show("Please enter username", self)
            return
        }
        if self.txtPassword.text == "" {
            Alert.show("Please enter password", self)
            return
        }
        if self.txtFirstName.text == "" {
            Alert.show("Please enter firstname", self)
            return
        }
        if self.txtLastName.text == "" {
            Alert.show("Please enter lastname", self)
            return
        }
        if self.txtPhoneNumber.text == "" {
            Alert.show("Please enter phone", self)
            return
        }
        
        let networkRachable: Reachability = Reachability.forInternetConnection()
        let networkStatus: NetworkStatus = networkRachable.currentReachabilityStatus()
        if networkStatus.rawValue == 0
        {
            Alert.show("No internet connection", self)
            return
        }
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + register
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(self.txtEmail.text, forKey: "email")
        parameter.setValue(self.txtUserName.text, forKey: "username")
        parameter.setValue(self.txtPassword.text, forKey: "password")
        parameter.setValue(self.txtFirstName.text, forKey: "firstname")
        parameter.setValue(self.txtLastName.text, forKey: "lastname")
        parameter.setValue(self.txtPhoneNumber.text, forKey: "mobile")
        
        print(parameter)
        
        var jsondata2 = Data()
        
        do {
            jsondata2 = try JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted)
            // use jsonData
        } catch {
            // report error
        }
        
        request.httpBody = jsondata2
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            SKActivityIndicator.dismiss()
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
            }
            
            let jsonResult = String(decoding: data, as: UTF8.self)
            
            if jsonResult != "\"\""
            {
                 Alert.show(jsonResult.replacingOccurrences(of: "\"", with: ""), self)
            }
            else
            {
                DispatchQueue.main.async()
                    {
                        
                        self.navigationController?.popViewController(animated: true)
                         Alert.show("Register successfully", self.obj!)
                }
            }
            
            print(jsonResult)
        })
        
        dataTask.resume()
        
    
    }
    
    //MARK:- Memory managment method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
