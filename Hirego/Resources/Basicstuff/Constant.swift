//
//  Constant.swift
//  GameON
//
//  Created by JD on 14/06/17.
//  Copyright © 2017 JDB Techs. All rights reserved.
//

import Foundation
import UIKit


let Default  = UserDefaults.standard

let Title = "Hirego"

let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

//var user:CurrentUser!

//let db:FMDatabase = FMDatabase(file: "gameon", ofType: "db")


let MainDomain = "https://hgo-dev.azurewebsites.net/"

//let HOSTNAME = "18.220.26.247"


let login = ".auth/login/custom"
let register = ".auth/login/register"
let listingsRecent = "api/listings?type=1&start=0&limit=5&args="
let listingsFeatured = "api/listings?type=0&start=0&limit=5&args="
let listingsYoumayAlsolike = "api/listings?type=2&start=0&limit=5&args="
let listTopDestination = "api/topplaces?start=0&limit=5&args="
let detail = "api/listing?id="
let addCar = "api/listings"
let getMake = "api/makes"
let getModels = "/api/models?makeid="
let getLatlong = "http://api.postcodes.io/postcodes"
let submitlist = "api/listings"

let getDropDowns = "user/category_list"
let addPost = "user/post_add"
let discover = "user/user_post_discover"
let fbLogin = "user/login_with_facebook"
let forgotPass = "user/forgot_password"
let myPost = "user/my_post"
let addRemoveFav = "user/add_remove_favorite"
let updateProfile = "user/update_profile"
let deleteAdd = "user/delete_adds"
let dltProfile = "user/delete_profile"
let changePwd = "user/change_password"
let favourite = "user/my_favorite_post_list"
let expired = "user/my_expired_post"
let reInstallAd = "user/re_active_post"
let chatList = "user/chat_list"
let chatDetail = "user/chat_detail"
let sendMessage = "user/chat_messageing"
let chatListPostwise = "user/chat_list_post_wise"
let resetBadge = "user/reset_badge"
let googleLogin = "LogingWithGoogle"
let UpdateFlightAttendant = "UpdateFlightAttendant"
let notificationSet = "user/user_package_notification_set"
let locationUpdate = "user/driver_location_update"

let deletechat = "user/delete_chat_conversation"

/*private let PasswordString =  String(format: "game_on:game_on")
 private let PasswordData = PasswordString.data(using: .utf8)
 private let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
 
 
 
 let base64Credential = "Basic \(base64EncodedCredential)"
 let Authorization = "Authorization"*/
