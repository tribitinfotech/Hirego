//
//  Carrousel.swift
//  ZCScrollMenu
//
//  Created by Ricardo Zertuche on 2/8/15.
//  Copyright (c) 2015 Ricardo Zertuche. All rights reserved.
//

import UIKit

@objc protocol ZCarouselDelegate {
    func ZCarouselShowingIndex(_ scrollView:ZCarousel, index: Int)
}

class ZCarousel: UIScrollView, UIScrollViewDelegate {
    
    var ZCButtons = [UIButton]()
    var ZCImages = [UIImageView]()
    private var buttons = [UIButton]()
    private var images = [UIView]()
    private var page: CGFloat?
    private var isImage: Bool?
    private var originalArrayCount: Int?
    
    var ZCdelegate: ZCarouselDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initalizeScrollViewProperties()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.initalizeScrollViewProperties()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initalizeScrollViewProperties()
    }
    
    func initalizeScrollViewProperties(){
        super.isPagingEnabled = true
        super.contentSize = CGSize(width: 0, height: self.frame.height)
        super.clipsToBounds = false
        super.delegate = self
        super.showsHorizontalScrollIndicator = false
        isImage = false
    }
    
    /*func addButtons(_ titles: [String]){
        originalArrayCount = titles.count
        //1
        var buttonFrame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.height)
        
        //a
        var finalButtons = titles
        //b
        
        if titles.count > 2 {
            //c
            finalButtons.insert(titles[titles.count-2], at: 0)
            
            if let lastItem = titles.last {
                finalButtons.insert(lastItem, at: 1)
            }
            
            finalButtons.append(titles[0])
            finalButtons.append(titles[1])
        }
        
        //2
        for i in 0..<finalButtons.count {
            //3
            //println("\(i) - \(finalButtons[i])")
            if i != 0 {
                buttonFrame = CGRect(x: buttonFrame.origin.x+buttonFrame.width, y: self.frame.height/2-self.frame.height/2, width: self.frame.width, height: self.frame.height)
            }
            //4
            let button = UIButton(frame: buttonFrame)
            button.setTitle(finalButtons[i], for: .normal)
            button.setTitleColor(UIColor.black, for: .normal)
            //6
            self.addSubview(button)
            self.contentSize.width = super.contentSize.width+button.frame.width
            
            buttons.append(button)
        }
        
        let middleButton = buttons[(buttons.count/2)]
        self.scrollRectToVisible(middleButton.frame, animated: false)
    }*/
    
    func addImages(_ imagesToUse: [UIView]){
        originalArrayCount = imagesToUse.count
        //1
        var imageViewFrame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.height)
       
        //2
        for i in 0..<imagesToUse.count {
            
            let dealView = imagesToUse[i] as! Deals
            
            if i != 0 {
                imageViewFrame = CGRect(x: imageViewFrame.origin.x+imageViewFrame.width, y: self.frame.height/2-self.frame.height/2, width: self.frame.size.width, height: self.frame.height)
            }
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapdealView(_:)))
            tapGesture.accessibilityValue = "\(i)"
            dealView.addGestureRecognizer(tapGesture)
            
            dealView.frame = imageViewFrame
            dealView.tag = i
            self.addSubview(dealView)
            
            self.contentSize.width = super.contentSize.width+dealView.frame.width
            //seprintlf.contentSize.width = dealView.frame.width*CGFloat(imagesToUse.count)
            images.append(dealView)
        }
        
        isImage = true
        
        let middleImage = images[(images.count/2)]
        self.scrollRectToVisible(middleImage.frame, animated: false)
        //self.ZCdelegate?.ZCarouselShowingIndex(self, index: images.count/2)
    }
    @objc func tapdealView(_ sender: UITapGestureRecognizer) {
        print("tapdealView")
        
        self.ZCdelegate?.ZCarouselShowingIndex(self, index: Int(sender.accessibilityValue!)!)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //1
        page = scrollView.contentOffset.x / self.frame.width
        //2
        var objectCount : CGFloat = 0
        var objects = [UIView]()
        if isImage==true {
            objectCount = CGFloat(images.count)
            objects = images
        }
        else {
            objectCount = CGFloat(buttons.count)
            objects = buttons
        }
        
        /*if let page = page {
            //3
            if page <= 1
            {
                print(objects)
                print(objectCount)
                print(images.count)
                let scrollToObject: AnyObject = objects[Int(objectCount-CGFloat(images.count))]
                print(scrollToObject.frame)
                self.scrollRectToVisible(scrollToObject.frame, animated: false)
            }
            if page >= (objectCount - CGFloat(self.images.count - 1)){
                let scrollToObject: AnyObject = objects[self.images.count - 1]
                self.scrollRectToVisible(scrollToObject.frame, animated: false)
            }
        }*/
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //1
        page = scrollView.contentOffset.x / self.frame.width
        //2
        
        if let page = page, let originalArrayCount = originalArrayCount {
            var pageInt = Int(Darwin.round(Float(page))) - (self.images.count - 1)
            
            if pageInt == -1 {
                pageInt = pageInt + originalArrayCount
            }
            
            if pageInt == originalArrayCount {
                pageInt = 0
            }
            
            pageInt = Int(page)
            
            let scrollToObject: AnyObject = images[pageInt]
            print(scrollToObject.frame)
            self.scrollRectToVisible(scrollToObject.frame, animated: false)
            
           // self.ZCdelegate?.ZCarouselShowingIndex(self, index: pageInt)
        }
    }
}
