//
//  EnterInfoViewController.swift
//  Hirego
//
//  Created by Jitendra bhadja on 23/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit

class EnterInfoViewController: UIViewController,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate {

    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtPlaceHolder: UITextField!
    var dictInfo:NSDictionary?
    
    @IBOutlet weak var txtSeats: UITextField!
    @IBOutlet weak var txtDailyRate: UITextField!
    
    @IBOutlet weak var txtPostCode: UITextField!
    @IBOutlet weak var txtHourlyRate: UITextField!
    
    @IBOutlet weak var btnImage: UIButton!
    
    var imageSelected:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtDescription.layer.cornerRadius = 2.0
        self.txtDescription.layer.borderWidth = 1.0
        self.txtDescription.layer.borderColor = UIColor.black.cgColor
        
        self.btnImage.layer.cornerRadius = 7
        // Do any additional setup after loading the view.
    }

    //MARK:- Action zones
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCameraAction(_ sender: Any)
    {
        let actionSheet = UIActionSheet(title: "Select option", delegate:self , cancelButtonTitle: "Cancel", destructiveButtonTitle:nil)
        actionSheet.addButton(withTitle: "Gallery")
        actionSheet.addButton(withTitle: "Camera")
        actionSheet.show(in: self.view)
    }
    
    @IBAction func btnNextAction(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if txtDailyRate.text == "" {
            
            Alert.show("Please enter daily rate", self)
            return
        }
        if txtPostCode.text == "" {
            
            Alert.show("Please enter postcode", self)
            return
        }
        if txtSeats.text == "" {
            
            Alert.show("Please enter seats", self)
            return
        }
        if txtDescription.text == "" {
            
            Alert.show("Please enter description", self)
            return
        }
        
        if self.imageSelected == true {
            
            self.dictInfo?.setValue(self.btnImage.image(for: .normal), forKey: "image")
        }
        
        self.dictInfo?.setValue(self.txtDailyRate.text, forKey: "dailyRate")
        self.dictInfo?.setValue(self.txtHourlyRate.text, forKey: "hourlyRate")
        self.dictInfo?.setValue(self.txtPostCode.text, forKey: "postcode")
        self.dictInfo?.setValue(self.txtSeats.text, forKey: "seats")
        self.dictInfo?.setValue(self.txtDescription.text, forKey: "description")
        
        let obj:EnterAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "EnterAddressViewController") as! EnterAddressViewController
        obj.dictInfo = self.dictInfo
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    //MARK:- UIImagepicker delegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        self.imageSelected = true
        
        let image:UIImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        self.btnImage.setImage(image, for: .normal)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:UIActionSheetDelegate
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 1  {
            let controller = UIImagePickerController()
            controller.allowsEditing = true
            controller.sourceType = .photoLibrary
            controller.delegate = self
            self.present(controller, animated: true, completion: nil)
            print("gallary")
        }
        if buttonIndex == 2 {
            let controller = UIImagePickerController()
            controller.allowsEditing = true
            controller.sourceType = .camera
            controller.delegate = self
            self.present(controller, animated: true, completion: nil)
            print("camera")
        }
    }
    
    //MARK:- Textview delegate
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.txtPlaceHolder.text = textView.text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
