//
//  EnterAddressViewController.swift
//  Hirego
//
//  Created by Jitendra bhadja on 24/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView
import SDWebImage


class EnterAddressViewController: UIViewController {

    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    
    var dictInfo:NSDictionary?
    
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    var imageURL:String = ""
    
    var imageHash:String = ""
    
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let dataProfile:NSData = UserDefaults.standard.object(forKey: "userData") as! NSData
        
        self.dictUser =  NSMutableDictionary.init(dictionary: NSKeyedUnarchiver.unarchiveObject(with: dataProfile as Data as Data) as! NSDictionary)
        
        print(self.dictUser)
        // Do any additional setup after loading the view.
    }

    //MARK:- Private method
    func submitCar()
    {
        
        let strURL:String = MainDomain + submitlist
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "POST"
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        parameter.setValue(self.latitude, forKey: "latitude")
        parameter.setValue(self.longitude, forKey: "longitude")
        parameter.setValue(self.dictInfo?.value(forKey: "hourlyRate"), forKey: "hourlyrate")
        parameter.setValue(self.dictInfo?.value(forKey: "dailyRate"), forKey: "dailyrate")
        parameter.setValue(self.dictInfo?.value(forKey: "makeID"), forKey: "makeid")
        parameter.setValue(self.dictInfo?.value(forKey: "modelID"), forKey: "modelid")
        parameter.setValue(self.dictInfo?.value(forKey: "seats"), forKey: "seats")
        parameter.setValue(self.dictInfo?.value(forKey: "registrationNumber"), forKey: "registration")
        parameter.setValue(self.imageHash.replacingOccurrences(of: "\n", with: ""), forKey: "imagehash")
        parameter.setValue(self.txtCity.text, forKey: "city")
        parameter.setValue(self.txtLocation.text, forKey: "locationname")
        parameter.setValue(self.dictInfo?.value(forKey: "postcode"), forKey: "postcode")
        parameter.setValue(self.dictInfo?.value(forKey: "description"), forKey: "description")
        
        print(parameter)
        
        var jsondata2 = Data()
        
        do {
            jsondata2 = try JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted)
            // use jsonData
        } catch {
            // report error
        }
        request.httpBody = jsondata2
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                SKActivityIndicator.dismiss()
                
                let strResponse:String = String(decoding: data, as: UTF8.self)
                
                if strResponse.contains("Success") == true
                {
                    for nav in (self.navigationController?.viewControllers)!
                    {
                        print(nav)
                        if nav.isKind(of: MenuController.self)
                        {
                            self.navigationController?.popToViewController(nav, animated: false)
                            
                        }
                    }
                }
            }
        })
        
        dataTask.resume()
        
        
    }
    func upload(url:URL)
    {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
  
            multipartFormData.append(UIImageJPEGRepresentation(self.dictInfo?.value(forKey: "image") as! UIImage, 0.5)!, withName: "filetoupload", fileName: "filetoupload.jpg",mimeType: "image/jpeg")
           
            
        }, to: "http://104.46.39.118:3000/image") { (encodingResult) in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request ?? "")  // original URL request
                    print(response.response ?? "") // URL response
                    print(response.data ?? "")     // server data
                    print(response.result)

                    let strImage:String = String(decoding: response.data!, as: UTF8.self)
                    
                    print(strImage)
                    
                    let arr:NSArray = (strImage.components(separatedBy: " ") as? NSArray)!
                    
                    if arr.count > 1
                    {
                        self.imageHash = "\(arr.object(at: 1) as! String)"
                    }

                    self.submitCar()
                    
                }
            case .failure(let encodingError):
                print(encodingError)
                SKActivityIndicator.dismiss()
            }
        }
        
        
    }
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = UIImagePNGRepresentation(image)!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    func uploadImage(image:UIImage)
    {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        // choose a name for your image
        let fileName = "image.jpg"
        // create the destination file url to save your image
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        // get your UIImage jpeg data representation and check if the destination file url already exists
        let data = UIImageJPEGRepresentation(image, 1.0)
            do {
                // writes the image data to disk
                try data?.write(to: fileURL)
                self.upload(url: fileURL)
                print("file saved")
            } catch {
                print("error saving file:", error)
            }
        

    }
    func getLatitudeFromPostcode()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = getLatlong
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "POST"

        request.addValue("b6885888-569d-fb7a-6cdf-84464e646d2a", forHTTPHeaderField: "Postman-Token")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        let arr:[String] = [self.dictInfo?.value(forKey: "postcode") as! String]
        parameter.setValue(arr, forKey: "postcodes")
        print(parameter)
        
        var jsondata2 = Data()
        
        do {
            jsondata2 = try JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted)
            // use jsonData
        } catch {
            // report error
        }
        request.httpBody = jsondata2
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let dictResult:NSMutableDictionary = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
                    
                    print(dictResult)
                    
                    if let dict =  (dictResult.value(forKey: "result") as! NSMutableArray).object(at: 0) as? NSMutableDictionary
                    {
                    
                        if let dictInfo =  dict.value(forKey: "result") as? NSMutableDictionary
                        {
                            self.latitude = dictInfo.value(forKey: "latitude") as! Double
                            self.longitude = dictInfo.value(forKey: "longitude") as! Double
                            
                            print(self.latitude)
                            print(self.longitude)
                        }
                        else
                        {
                            
                        }
                        
                        if let image = self.dictInfo?.value(forKey: "image") as? UIImage
                        {
                            self.uploadImage(image: image)
                        }
                        else
                        {
                            self.submitCar()
                        }
                    }
                    
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
        })
        
        dataTask.resume()
        
        
    }
    
    //MARK:- Action zones
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitAction(_ sender: Any)
    {
        if txtCity.text == "" {
            
            Alert.show("Please enter city", self)
            return
        }
        if txtLocation.text == "" {
            
            Alert.show("Please enter location", self)
            return
        }
        
        self.dictInfo?.setValue(self.txtCity.text, forKey: "city")
        self.dictInfo?.setValue(self.txtLocation.text, forKey: "location")
        
        print(self.dictInfo ?? "")
        
        self.getLatitudeFromPostcode()
        
    }
    
    
    //MARK:- Memory managment method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
