//
//  SelectModelViewController.swift
//  Hirego
//
//  Created by Jitendra bhadja on 23/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView
import SDWebImage

class SelectModelViewController: UIViewController,UITextFieldDelegate,DropDowndelegate {

    @IBOutlet weak var txtRegistration: UITextField!
    
    @IBOutlet weak var txtModel: UITextField!
    @IBOutlet weak var txtMake: UITextField!
    
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    
    var arrMake:NSMutableArray = NSMutableArray()
    var arrModel:NSMutableArray = NSMutableArray()
    
    var currentTextfieldID:NSInteger = 0
    
    var strMakeID:String = ""
    var strModelID:String = ""
    
     var dropDown:DKDropDownView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dropDown = Bundle.main.loadNibNamed("DKDropDownView", owner: nil, options: nil)?[0] as? DKDropDownView
        self.dropDown?.datePicker.isHidden = true
        self.dropDown?.pickerView.isHidden = false
        self.dropDown?.datePicker.datePickerMode = .dateAndTime
        self.dropDown?.formate = "EEE, MMM dd, hh:mm a"
        self.dropDown?.delegate = self
        self.dropDown?.datePicker.locale = Locale(identifier: "en_US_POSIX")
        
        dropDown?.textfield = self.txtMake
        self.txtMake.inputView = dropDown
        self.txtMake.tintColor = UIColor.white
        
        let dataProfile:NSData = UserDefaults.standard.object(forKey: "userData") as! NSData
        
        self.dictUser =  NSMutableDictionary.init(dictionary: NSKeyedUnarchiver.unarchiveObject(with: dataProfile as Data as Data) as! NSDictionary)
        
        print(self.dictUser)
        
        self.getMakes()

        // Do any additional setup after loading the view.
    }
    //MARK:- Private method
    
    func getModel()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + getModels + self.strMakeID
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    self.arrModel = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    SKActivityIndicator.dismiss()
                    
                    print(self.arrModel)
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
        })
        
        dataTask.resume()
        
        
    }
    func getMakes()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + getMake
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    self.arrMake = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
       
                    SKActivityIndicator.dismiss()
                    
                    print(self.arrMake)
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
        })
        
        dataTask.resume()
        
        
    }
    
    //MARK:- Textfield delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
        if textField.tag == 1 {
        
            dropDown?.textfield = self.txtMake
            self.txtMake.inputView = dropDown
            self.txtMake.tintColor = UIColor.white
            
            self.dropDown?.lblDate.text = "Select Make"
            self.dropDown?.inputArray = self.arrMake
            self.currentTextfieldID = 1
        }
        if textField.tag == 2 {
        
             dropDown?.textfield = self.txtModel
             self.txtModel.inputView = dropDown
             self.txtModel.tintColor = UIColor.white
            
             self.dropDown?.lblDate.text = "Select Model"
             self.dropDown?.inputArray = self.arrModel
             self.currentTextfieldID = 2
        }
        return true
    }
    func textFieldDidPressCancle(textField: UITextField) {
        
    }
    func textFieldDidPressDone(textField: UITextField, index: Int) {
        
        if self.currentTextfieldID == 1{
            
            textField.text = ((self.arrMake.object(at: index) as! NSDictionary).mutableCopy() as! NSMutableDictionary).value(forKey: "Name") as? String
            
            self.strMakeID = ((self.arrMake.object(at: index) as! NSDictionary).mutableCopy() as! NSMutableDictionary).value(forKey: "Id") as! String
            
            self.getModel()
            
            self.strModelID = ""
            
            self.txtModel.text = ""
            
        }
        if self.currentTextfieldID == 2{
            
            textField.text = ((self.arrModel.object(at: index) as! NSDictionary).mutableCopy() as! NSMutableDictionary).value(forKey: "Name") as? String
            
            self.strModelID = ((self.arrModel.object(at: index) as! NSDictionary).mutableCopy() as! NSMutableDictionary).value(forKey: "Id") as! String
        }
        
    }
    
    
    //MARK:- Action zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNextAction(_ sender: Any)
    {
        if txtMake.text == "" {
            
            Alert.show("Please select make", self)
            return
        }
        if txtModel.text == "" {
            
            Alert.show("Please select model", self)
            return
        }
        if txtRegistration.text == "" {
            
            Alert.show("Please enter registration number", self)
            return
        }
        
        self.view.endEditing(true)
        
        let dict:NSMutableDictionary = NSMutableDictionary()
        dict.setValue(self.strMakeID, forKey: "makeID")
        dict.setValue(self.strModelID, forKey: "modelID")
        dict.setValue(self.txtRegistration.text, forKey: "registrationNumber")
        
        let obj:EnterInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "EnterInfoViewController") as! EnterInfoViewController
        obj.dictInfo = dict
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    //MARK:- Memory managment method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
