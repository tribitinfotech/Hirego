//
//  Deals.swift
//  Hirego
//
//  Created by Jitendra bhadja on 26/06/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit

class Deals: UIView {

    @IBOutlet weak var lblSeats: UILabel!
    @IBOutlet weak var rating: BSRating!
    @IBOutlet weak var imgDeal: UIImageView!
    @IBOutlet weak var lblNumberOfDays: UILabel!
    @IBOutlet weak var lblStaticPerDay: UILabel!
    @IBOutlet weak var lblSavePercentage: UILabel!
    @IBOutlet weak var lblTrips: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var imgDiscountDeal: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()

        self.rating.setRatingColor(UIColor.init(red: 255.0/255.0, green: 213.0/255.0, blue: 43.0/255.0, alpha: 1.0))
        self.rating.setFilledValue(3.0)
        self.rating.setRatingType(BSRatingTypeGrayed)
      
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
