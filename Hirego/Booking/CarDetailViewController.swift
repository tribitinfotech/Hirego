//
//  CarDetailViewController.swift
//  Hirego
//
//  Created by Jitendra bhadja on 27/06/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView
import SDWebImage
class CarDetailViewController: UIViewController,ZCarouselDelegate,DropDowndelegate,UITextFieldDelegate {

    @IBOutlet weak var rateUser: BSRating!
    @IBOutlet weak var viewLike: UIView!
    
    var strId:String?
    
    var youmayAlsoLike: ZCarousel?
    var arrYouMayAlsoLike:NSMutableArray = NSMutableArray()
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    var dictDetail:NSMutableDictionary = NSMutableDictionary()
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var lblPricePerDay: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblSeats: UILabel!
    @IBOutlet weak var vehicleRating: BSRating!
    
    @IBOutlet weak var lblInfoCarOwner: UILabel!
    @IBOutlet weak var infoOwnerRating: BSRating!
    @IBOutlet weak var lblCarDescription: UILabel!
    
    @IBOutlet weak var txtChange: UITextField!
    @IBOutlet weak var lblStartDate: UILabel!
    
    @IBOutlet weak var lblEndDate: UILabel!
    
    var isStartDateSelection:Bool = false
    
    var dropDownView:DKDropDownView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dropDownView = Bundle.main.loadNibNamed("DKDropDownView", owner: nil, options: nil)?[0] as? DKDropDownView
        self.dropDownView?.datePicker.isHidden = false
        self.dropDownView?.pickerView.isHidden = true
        self.dropDownView?.datePicker.datePickerMode = .dateAndTime
        self.dropDownView?.formate = "EEE, MMM dd, hh:mm a"
        self.dropDownView?.delegate = self
        self.dropDownView?.datePicker.locale = Locale(identifier: "en_US_POSIX")
        
        dropDownView?.textfield = self.txtChange
        self.txtChange.inputView = dropDownView
        self.txtChange.tintColor = UIColor.white
        
       self.setupforRecommended()
        
       self.getCarDetail()
        // Do any additional setup after loading the view.
    }

    //MARK:- UITextfield delagete
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if self.isStartDateSelection == true
        {
            self.isStartDateSelection = false
            
            self.dropDownView?.lblDate.text = "Select End Date"
        }
        else if self.isStartDateSelection == false
        {
            self.isStartDateSelection = true
            
            self.dropDownView?.lblDate.text = "Select Start Date"
        }
        
        
        return true
    }
    
    func textFieldDidPressDone(textField: UITextField, date: Date) {
        print(date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, MMM dd, hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        
        print(dateFormatter.string(from: date))
        
        if self.isStartDateSelection == true  {
            
            self.lblStartDate.text = dateFormatter.string(from: date)
            
            self.txtChange.becomeFirstResponder()
            
        }
        else
        {
            self.lblEndDate.text = dateFormatter.string(from: date)
        }
        
        
        
    }
    
    //MARK:- Private method
    func setupforRecommended()
    {
        //You may also like
        
        if self.arrYouMayAlsoLike.count > 0 {
            
            youmayAlsoLike = ZCarousel(frame: CGRect( x: 20,
                                                      y: 0,
                                                      width: self.view.frame.size.width - 40,
                                                      height: 255))
            
            youmayAlsoLike?.tag = 101
            
            youmayAlsoLike?.ZCdelegate = self
            
            youmayAlsoLike?.backgroundColor = UIColor.white
            
            var arrLike:[UIView] = []
            
            for i in 0..<self.arrYouMayAlsoLike.count
            {
                let dealView = Bundle.main.loadNibNamed("Deals", owner: nil, options: nil)?[0] as! Deals
                dealView.lblCarName.text = "\((self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "Make") as! String)"
                dealView.imgDeal.sd_setImage(with: URL.init(string: "http://104.46.39.118:8080/ipfs/\(((self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "ImageHash") as? String)!)"), placeholderImage: UIImage.init(named: "car_placeholder.png"), options: .lowPriority, completed: nil)
                dealView.lblNumberOfDays.text = "\((self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "PricePerDay") as! NSNumber)"
                if let value = (self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "VehicleRating") as? Float {
                    
                    dealView.rating.setFilledValue(value)
                }
                else
                {
                    dealView.rating.setFilledValue(0)
                }
                dealView.lblNumberOfDays.adjustsFontSizeToFitWidth = true
                dealView.lblSeats.text = "\((self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "Seats") as! NSNumber) Seats"
                arrLike.append(dealView)
            }
            
            youmayAlsoLike?.addImages(arrLike)
            
            if let images = youmayAlsoLike {
                self.viewLike.addSubview(images)
            }
            
        }
    }
    func getCarDetail()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + detail + self.strId!
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    SKActivityIndicator.dismiss()
                    
                    self.dictDetail = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
                    
                    print(self.dictDetail)
                    
                    self.setDetails()
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
        })
        
        dataTask.resume()
        
        
    }
    func setDetails()
    {
        self.imgCar.sd_setImage(with: URL.init(string: "http://104.46.39.118:8080/ipfs/\((self.dictDetail.value(forKey: "ImageHash") as? String)!)"), placeholderImage: UIImage.init(named: "car_placeholder.png"), options: .lowPriority, completed: nil)
        self.lblOwnerName.text = self.dictDetail.value(forKey: "OwnerId") as? String
        self.lblCarName.text = self.dictDetail.value(forKey: "Make") as? String
        self.lblPricePerDay.text = "\(self.dictDetail.value(forKey: "PricePerDay") as! NSNumber)"
        
        self.lblSeats.text = "\(self.dictDetail.value(forKey: "Seats") as! NSNumber) Seats"
        
        if let value = self.dictDetail.value(forKey: "VehicleRating") as? Float {
            
            self.vehicleRating.setFilledValue(value)
        }
        else
        {
            self.vehicleRating.setFilledValue(0)
        }
        
        if let value = self.dictDetail.value(forKey: "OwnerRating") as? Float {
            
            self.infoOwnerRating.setFilledValue(value)
        }
        else
        {
            self.infoOwnerRating.setFilledValue(0)
        }
        
        self.lblInfoCarOwner.text = self.dictDetail.value(forKey: "OwnerId") as? String
        
        self.lblCarDescription.text = self.dictDetail.value(forKey: "Description") as? String
        
    }
    //MARK:- Action zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCheckoutAction(_ sender: Any)
    {
        let obj:CheckoutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnShareAction(_ sender: Any)
    {
        let shareText = "Download application from URL"

        let url:URL = URL.init(string: "https://itunes.apple.com/us/app/hungry-harry-free/id1155412392?ls=1&mt=8")!
        
        // set up activity view controller
        let imageToShare = [ shareText,url ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK:- Zcoursal delegate
    
    func ZCarouselShowingIndex(_ scrollView: ZCarousel, index: Int) {
        
        let obj:CarDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailViewController") as! CarDetailViewController
        
        if scrollView == youmayAlsoLike {
            print("Showing youmayAlsoLike at index \(index)")
            obj.strId = (self.arrYouMayAlsoLike.object(at: index) as? NSMutableDictionary)?.value(forKey: "Id") as? String
        }
        obj.dictUser = self.dictUser
        obj.arrYouMayAlsoLike = self.arrYouMayAlsoLike
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    //MARK:- Memory managment
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
