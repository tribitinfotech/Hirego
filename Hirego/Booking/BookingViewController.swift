//
//  MenuViewController.swift
//  Hirego
//
//  Created by Jitendra bhadja on 26/06/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView
import SDWebImage
import TPKeyboardAvoiding
class BookingViewController: UIViewController,ZCarouselDelegate,ZCarouselDelegateDest {

    @IBOutlet weak var scrlView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var viewSearch: UIView!
    
    @IBOutlet weak var viewDeals: UIView!
    @IBOutlet weak var viewRecentViewed: UIView!
    @IBOutlet weak var viewFeaturedCars: UIView!
    @IBOutlet weak var viewTopDestination: UIView!
    @IBOutlet weak var viewLike: UIView!
    
    var deals: ZCarousel?
    var recentViewed: ZCarousel?
    var featuredCars: ZCarousel?
    var youmayAlsoLike: ZCarousel?
    var topDestination: ZCarouselDestination?
    
    var arrBestDeals:NSMutableArray = NSMutableArray()
    var arrRecentlyViewed:NSMutableArray = NSMutableArray()
    var arrFeaturedCar:NSMutableArray = NSMutableArray()
    var arrTopDestination:NSMutableArray = NSMutableArray()
    var arrYouMayAlsoLike:NSMutableArray = NSMutableArray()
    
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    
    var arrDeals:[String] = ["AMG TOYOTA CAR132","BMW Car 2017","TOYOTA CAR 2017","BMW Car 2017","AMG TOYOTA CAR","BMW Car 2017","TOYOTA CAR 2017","BMW Car 2017"]
    
    var totalCount:NSInteger = 0
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.checkTimeStmapValidation()
        
       
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.scrlView.contentInsetAdjustmentBehavior = .never
        self.scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(callWebserviceAfterRenewToken), name: Notification.Name("callWebserviceAfterRenewToken"), object: nil)
        
        let dataProfile:NSData = UserDefaults.standard.object(forKey: "userData") as! NSData
        
        self.dictUser =  NSMutableDictionary.init(dictionary: NSKeyedUnarchiver.unarchiveObject(with: dataProfile as Data as Data) as! NSDictionary)
        
        print(self.dictUser)
        
        self.viewSearch.layer.cornerRadius = 2.0
 
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Private method
    @objc func callWebserviceAfterRenewToken()
    {
        SKActivityIndicator.dismiss()
        
        self.totalCount = 0
        
        self.getRecentViewed()
        
        self.getFeatured()
        
        self.getyouMayAlsoLike()
        
        self.getTopDestination()
    }
    func checkTimeStmapValidation()
    {
        if let username = UserDefaults.standard.value(forKey: "username") as? String
        {
            print(username)
            
            let firstPress = UserDefaults.standard.value(forKey: "timeStamp")
            
            let date = Date()
            
            let secondPress = date.timeIntervalSinceReferenceDate //the second button press
            
            let firstti = NSInteger(firstPress as! TimeInterval)
            let secondti = NSInteger(secondPress )
            
            let diffInSeconds = secondti - firstti //total time difference in seconds
            
            let hours = diffInSeconds/60/60 //hours=diff in seconds / 60 sec per min / 60 min per hour
            
            if hours >= 24 {
                
                SKActivityIndicator.spinnerStyle(.spinningCircle)
                SKActivityIndicator.show("", userInteractionStatus: true)
                
                AppDelegate.shared.loginUser()
                
            } else {
                self.totalCount = 0
                
                self.getRecentViewed()
                
                self.getFeatured()
                
                self.getyouMayAlsoLike()
                
                self.getTopDestination()
            }
            
        }
    }
    @objc func dismissHUD()
    {
        SKActivityIndicator.dismiss()
    }
    func getTopDestination()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + listTopDestination
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                self.totalCount = self.totalCount + 1
                
                do {
                    self.arrTopDestination = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(self.arrTopDestination)
                    
                    if self.totalCount == 4
                    {
                        self.setUpListings()
                    }
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
        })
        
        dataTask.resume()
        
        
    }
    func getyouMayAlsoLike()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + listingsYoumayAlsolike
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                self.totalCount = self.totalCount + 1
                
                do {
                    self.arrYouMayAlsoLike = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    self.arrBestDeals = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(self.arrYouMayAlsoLike)
                    
                    if self.totalCount == 4
                    {
                        self.setUpListings()
                    }
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
        })
        
        dataTask.resume()
        
        
    }
    func getRecentViewed()
    {

        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + listingsRecent
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
               
                return
            }
            
            DispatchQueue.main.async {
                
                self.totalCount = self.totalCount + 1
                
                do {
                    self.arrRecentlyViewed = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(self.arrRecentlyViewed)
                    
                    if self.totalCount == 4
                    {
                        self.setUpListings()
                    }

                    // use jsonData
                } catch {
                    // report error
                }
            }
            
           
            
        })
        
        dataTask.resume()
        
        
    }
    func getFeatured()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + listingsFeatured
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                SKActivityIndicator.dismiss()
                return
            }
            
            DispatchQueue.main.async {
                
                self.totalCount = self.totalCount + 1
                
                do {
                    self.arrFeaturedCar = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(self.arrFeaturedCar)
                    
                    if self.totalCount == 4
                    {
                        self.setUpListings()
                    }
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    func setUpListings()
    {
        self.viewDeals.removeAllSubView()
        self.viewRecentViewed.removeAllSubView()
        self.viewFeaturedCars.removeAllSubView()
        self.viewTopDestination.removeAllSubView()
        self.viewLike.removeAllSubView()
        
        //Best deals
        
        if self.arrBestDeals.count > 0 {
            
            deals = ZCarousel(frame: CGRect( x: 20,
                                             y: 0,
                                             width: self.view.frame.size.width - 40,
                                             height: 255))
            
            deals?.ZCdelegate = self
            
            deals?.backgroundColor = UIColor.white
            
            var arrDeal:[UIView] = []
            
            for i in 0..<self.arrBestDeals.count
            {
                let dealView = Bundle.main.loadNibNamed("Deals", owner: nil, options: nil)?[0] as! Deals
                dealView.lblCarName.text = "\((self.arrBestDeals.object(at: i) as! NSMutableDictionary).value(forKey: "Make") as! String)"
                dealView.imgDeal.sd_setImage(with: URL.init(string: "http://104.46.39.118:8080/ipfs/\(((self.arrBestDeals.object(at: i) as! NSMutableDictionary).value(forKey: "ImageHash") as? String)!)"), placeholderImage: UIImage.init(named: "car_placeholder.png"), options: .lowPriority, completed: nil)
                dealView.lblNumberOfDays.text = "\((self.arrBestDeals.object(at: i) as! NSMutableDictionary).value(forKey: "PricePerDay") as! NSNumber)"
                if let value = (self.arrBestDeals.object(at: i) as! NSMutableDictionary).value(forKey: "VehicleRating") as? Float {
                    
                    dealView.rating.setFilledValue(value)
                }
                else
                {
                    dealView.rating.setFilledValue(0)
                }
                dealView.lblNumberOfDays.adjustsFontSizeToFitWidth = true
                dealView.lblSeats.text = "\((self.arrBestDeals.object(at: i) as! NSMutableDictionary).value(forKey: "Seats") as! NSNumber) Seats"
                arrDeal.append(dealView)
            }
            
            deals?.addImages(arrDeal)
            
            if let images = deals {
                self.viewDeals.addSubview(images)
            }
     
            
        }
        
        
        //Recently viewed
        
        if self.arrRecentlyViewed.count > 0 {
            
            recentViewed = ZCarousel(frame: CGRect( x: 20,
                                                    y: 0,
                                                    width: self.view.frame.size.width - 40,
                                                    height: 255))
            
            recentViewed?.tag = 99
            
            recentViewed?.ZCdelegate = self
            
            recentViewed?.backgroundColor = UIColor.white
            
            var arrRecent:[UIView] = []
            
            for i in 0..<self.arrRecentlyViewed.count
            {
                let dealView = Bundle.main.loadNibNamed("Deals", owner: nil, options: nil)?[0] as! Deals
                dealView.lblCarName.text = "\((self.arrRecentlyViewed.object(at: i) as! NSMutableDictionary).value(forKey: "Make") as! String)"
                dealView.imgDeal.sd_setImage(with: URL.init(string: "http://104.46.39.118:8080/ipfs/\(((self.arrRecentlyViewed.object(at: i) as! NSMutableDictionary).value(forKey: "ImageHash") as? String)!)"), placeholderImage: UIImage.init(named: "car_placeholder.png"), options: .lowPriority, completed: nil)
                dealView.lblNumberOfDays.text = "\((self.arrRecentlyViewed.object(at: i) as! NSMutableDictionary).value(forKey: "PricePerDay") as! NSNumber)"
                if let value = (self.arrRecentlyViewed.object(at: i) as! NSMutableDictionary).value(forKey: "VehicleRating") as? Float {
                    
                    dealView.rating.setFilledValue(value)
                }
                else
                {
                    dealView.rating.setFilledValue(0)
                }
                dealView.lblNumberOfDays.adjustsFontSizeToFitWidth = true
                dealView.lblSeats.text = "\((self.arrRecentlyViewed.object(at: i) as! NSMutableDictionary).value(forKey: "Seats") as! NSNumber) Seats"
                arrRecent.append(dealView)
            }
            
            recentViewed?.addImages(arrRecent)
            
            if let images = recentViewed {
                self.viewRecentViewed.addSubview(images)
            }
       
        }
  
        
        //Featured cars
        
        if self.arrFeaturedCar.count > 0 {
            
            featuredCars = ZCarousel(frame: CGRect( x: 20,
                                                    y: 0,
                                                    width: self.view.frame.size.width - 40,
                                                    height: 255))
            
            featuredCars?.ZCdelegate = self
            
            featuredCars?.backgroundColor = UIColor.white
            
            var arrFeatured:[UIView] = []
            
            for i in 0..<self.arrFeaturedCar.count
            {
                let dealView = Bundle.main.loadNibNamed("Deals", owner: nil, options: nil)?[0] as! Deals
                dealView.lblCarName.text = "\((self.arrFeaturedCar.object(at: i) as! NSMutableDictionary).value(forKey: "Make") as! String)"
                dealView.imgDeal.sd_setImage(with: URL.init(string: "http://104.46.39.118:8080/ipfs/\(((self.arrFeaturedCar.object(at: i) as! NSMutableDictionary).value(forKey: "ImageHash") as? String)!)"), placeholderImage: UIImage.init(named: "car_placeholder.png"), options: .lowPriority, completed: nil)
                if let value = (self.arrFeaturedCar.object(at: i) as! NSMutableDictionary).value(forKey: "VehicleRating") as? Float {
                    
                    dealView.rating.setFilledValue(value)
                }
                else
                {
                    dealView.rating.setFilledValue(0)
                }
                dealView.lblNumberOfDays.text = "\((self.arrFeaturedCar.object(at: i) as! NSMutableDictionary).value(forKey: "PricePerDay") as! NSNumber)"
                dealView.lblNumberOfDays.adjustsFontSizeToFitWidth = true
                dealView.lblSeats.text = "\((self.arrFeaturedCar.object(at: i) as! NSMutableDictionary).value(forKey: "Seats") as! NSNumber) Seats"
                arrFeatured.append(dealView)
            }
            
            featuredCars?.addImages(arrFeatured)
            
            if let images = featuredCars {
                self.viewFeaturedCars.addSubview(images)
            }
         
        }
        
        
        
        //Top destination
        
        if self.arrTopDestination.count > 0 {
            
            topDestination = ZCarouselDestination(frame: CGRect( x: 20,
                                                                 y: 0,
                                                                 width: self.view.frame.size.width - 40,
                                                                 height: 160))
            
            topDestination?.ZCdelegate = self
            
            topDestination?.backgroundColor = UIColor.white
            
            var arrTopDestination:[UIView] = []
            
            for i in 0..<self.arrTopDestination.count
            {
                let dealView = Bundle.main.loadNibNamed("Destination", owner: nil, options: nil)?[0] as! Destination
                dealView.lblCity.text = "\((self.arrTopDestination.object(at: i) as! NSMutableDictionary).value(forKey: "Name") as! String)"
                dealView.lblCity.adjustsFontSizeToFitWidth = true
                dealView.imgDestination.sd_setImage(with: URL.init(string: "http://104.46.39.118:8080/ipfs/\(((self.arrTopDestination.object(at: i) as! NSMutableDictionary).value(forKey: "ImageHash") as? String)!)"), placeholderImage: nil, options: .lowPriority, completed: nil)
                arrTopDestination.append(dealView)
            }
            
            topDestination?.addImages(arrTopDestination)
            
            if let images = topDestination {
                self.viewTopDestination.addSubview(images)
            }
     
        }
        
        
        //You may also like
        
        if self.arrYouMayAlsoLike.count > 0 {
            
            youmayAlsoLike = ZCarousel(frame: CGRect( x: 20,
                                                      y: 0,
                                                      width: self.view.frame.size.width - 40,
                                                      height: 255))
            
            youmayAlsoLike?.tag = 101
            
            youmayAlsoLike?.ZCdelegate = self
            
            youmayAlsoLike?.backgroundColor = UIColor.white
            
            var arrLike:[UIView] = []
            
            for i in 0..<self.arrYouMayAlsoLike.count
            {
                let dealView = Bundle.main.loadNibNamed("Deals", owner: nil, options: nil)?[0] as! Deals
                dealView.lblCarName.text = "\((self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "Make") as! String)"
                dealView.imgDeal.sd_setImage(with: URL.init(string: "http://104.46.39.118:8080/ipfs/\(((self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "ImageHash") as? String)!)"), placeholderImage: UIImage.init(named: "car_placeholder.png"), options: .lowPriority, completed: nil)
                dealView.lblNumberOfDays.text = "\((self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "PricePerDay") as! NSNumber)"
                if let value = (self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "VehicleRating") as? Float {
                    
                    dealView.rating.setFilledValue(value)
                }
                else
                {
                    dealView.rating.setFilledValue(0)
                }
                dealView.lblNumberOfDays.adjustsFontSizeToFitWidth = true
                dealView.lblSeats.text = "\((self.arrYouMayAlsoLike.object(at: i) as! NSMutableDictionary).value(forKey: "Seats") as! NSNumber) Seats"
                arrLike.append(dealView)
            }
            
            youmayAlsoLike?.addImages(arrLike)
            
            if let images = youmayAlsoLike {
                self.viewLike.addSubview(images)
            }
    
        }
       
        self.perform(#selector(self.dismissHUD), with: nil, afterDelay: 0.0)
    }
    
    //MARK:- Action zone
    
    @IBAction func btnListYourCarAction(_ sender: Any)
    {
        let obj:SelectModelViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectModelViewController") as! SelectModelViewController
        self.navigationController?.pushViewController(obj, animated: true)
        
        self.scrlView.setContentOffset(.zero, animated: true)
        

    }
    
    
    //MARK:- Zcoursal delegate
    
    func ZCarouselShowingIndex(_ scrollView: ZCarousel, index: Int) {
        
        let obj:CarDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailViewController") as! CarDetailViewController
        
        if scrollView == deals {
            print("Showing deals at index \(index)")
            obj.strId = (self.arrBestDeals.object(at: index) as? NSMutableDictionary)?.value(forKey: "Id") as? String
        }
        if scrollView == recentViewed {
            print("Showing recentViewed at index \(index)")
            obj.strId = (self.arrRecentlyViewed.object(at: index) as? NSMutableDictionary)?.value(forKey: "Id") as? String
        }
        if scrollView == youmayAlsoLike {
            print("Showing youmayAlsoLike at index \(index)")
            obj.strId = (self.arrYouMayAlsoLike.object(at: index) as? NSMutableDictionary)?.value(forKey: "Id") as? String
        }
        if scrollView == featuredCars {
            print("Showing featuredCars at index \(index)")
            obj.strId = (self.arrFeaturedCar.object(at: index) as? NSMutableDictionary)?.value(forKey: "Id") as? String
        }
        obj.dictUser = self.dictUser
        obj.arrYouMayAlsoLike = self.arrYouMayAlsoLike
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    func ZCarouselShowingIndexDest(_ scrollView: ZCarouselDestination, index: Int) {
        
        if scrollView == topDestination {
            print("Showing topDestination at index \(index)")
        }
    }
    
    //MARK:- Memory managment

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
    func removeAllSubView()
    {
        for subView in self.subviews {
            if subView.tag != 100
            {
                subView.removeFromSuperview()
            }
        }
    }
}
